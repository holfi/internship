--Task 1
SELECT * FROM employees;

--Task 2
SELECT * FROM employees WHERE first_name = 'David';

--Task 3
SELECT * FROM employees WHERE job_id = 'IT_PROG';

--Task 4
SELECT * FROM employees WHERE department_id = 50 AND salary > 4000;

--Task 5
SELECT * FROM employees WHERE department_id = 20 OR department_id = 30;

--Task 6
SELECT * FROM employees WHERE first_name LIKE '%a';

--Task 7
SELECT * FROM employees WHERE department_id = 50 OR department_id = 80 AND commision_pct IS NOT NULL;

--Task 8
SELECT * FROM employees WHERE length(replace(first_name, 'n', 'nn')) - length(first_name) > 1;

--Task 9
SELECT * FROM employees WHERE length(first_name) > 4;

--Task 10
SELECT * FROM employees WHERE salary BETWEEN 8000 AND 9000;

--Task 11
SELECT * FROM employees WHERE first_name LIKE '%%%';
