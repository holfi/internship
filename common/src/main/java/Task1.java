import java.util.*;

public class Task1 {

    public static void main(String[] args) {
        boolean flag = false;

        List<Human> humanList = new ArrayList<>();
        humanList.add(new Human("Oleg", "Ryazanov", "Olegovich", 25, "M", "29.02.1995"));

        humanList.add(new Human("Yan", "Bomanov", "Yanovich", 18, "M", "14.09.2001"));
        humanList.add(new Human("Yan", "Bomanov", "Yanovich", 18, "M", "14.09.2001"));
        humanList.add(new Human("Yan", "Bomanov", "Yanovich", 18, "M", "14.09.2001"));
        humanList.add(new Human("Yan", "Bomanov", "Yanovich", 18, "M", "14.09.2001"));
//        humanList.add(new Human("Yan", "Bomanov", "Yanovich", 18, "M", "14.09.2001"));

        humanList.add(new Human("Yulia", "Fazillova", "Viktorovna", 21, "F", "24.10.1999"));
        humanList.add(new Human("Yulia", "Fazillova", "Viktorovna", 21, "F", "24.10.1999"));
        humanList.add(new Human("Yulia", "Fazillova", "Viktorovna", 21, "F", "24.10.1999"));
//        humanList.add(new Human("Yulia", "Fazillova", "Viktorovna", 21, "F", "24.10.1999"));

        humanList.add(new Human("Maxim", "Vikorskiy", "Maximovich", 33, "M", "01.01.1987"));
        humanList.add(new Human("Maxim", "Vikorskiy", "Maximovich", 33, "M", "01.01.1987"));
        humanList.add(new Human("Maxim", "Vikorskiy", "Maximovich", 33, "M", "01.01.1987"));

        // кладем объекты в Map для дальнейшего получения объекта и простого подсчета повторений
        Map<Human, Integer> humanMap = new HashMap<>();
        for (int i = 0; i < humanList.size(); i++) {
            Integer temp = humanMap.get(humanList.get(i));
            humanMap.put(humanList.get(i), temp == null ? 1 : temp + 1);
            if (humanMap.get(humanList.get(i)) > 3)
                flag = true;
        }

        // Подпункт 1
        // поиск совпадения по шаблону
        HumanUtil.searchCoincidence(humanList);

        // Подпункт 2
        // люди, у которых возраст не больше 20 и фамилия начинается на а, б, в, г или д
        HumanUtil.humanSoughtFor(humanList);

        // Подпункт 3
        // вывод повторяющихся объектов
        if(flag) {
            // коллекция для дубликатов
            List<Human> duplicates = HumanUtil.findRepeatablesInCollection(humanMap);
        } else {
            HumanUtil.findRepeatables(humanMap);
        }
    }

}
