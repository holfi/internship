import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class HumanUtil {

    // возвращает объект для создания шаблона
    public static Human getNewHuman(String firstName, String lastName, String patronymic, int age){
        return new Human(firstName, lastName, patronymic, age, "M", "29.02.1995");
    }

    // Подпункт 1
    // поиск совпадения по шаблону
    public static void searchCoincidence(List<Human> humanList) {
        humanList.stream().filter(s -> s.equals(HumanUtil.getNewHuman("Oleg", "Ryazanov", "Olegovich", 25)))
                .forEach(s -> System.out.println("Найдено совпадение:\n" + s + "\n"));
    }

    // Подпункт 2
    // люди, у которых возраст не больше 20 и фамилия начинается на а, б, в, г или д
    public static void humanSoughtFor(List<Human> humanList) {
        humanList.stream().filter(s -> (s.getAge() <= 20 &&
                "abvgd".contains(s.getLastName().toLowerCase().charAt(0) + "")))
                .limit(1)
                .forEach(s -> System.out.println("Человек, у которого возраст меньше 20 и фамилия начинается на а, б, в, г или д:\n" + s + "\n"));
    }

    // Подпункт 3
    // вывод повторяющихся объектов в отдельную коллекцию
    public static List<Human> findRepeatablesInCollection(Map<Human, Integer> humanMap) {
        List<Human> duplicates = new ArrayList<>();

        System.out.println("Вывод повторяющихся объектов в отдельной коллекции:");
        humanMap.entrySet().stream()
                .filter(s -> s.getValue() > 3)
                .map(Map.Entry::getKey)
                .peek(duplicates::add)
                .forEach(System.out::println);

        return duplicates;
    }

    // Подпункт 3
    // вывод повторяющихся объектов
    public static void findRepeatables(Map<Human, Integer> humanMap) {
        AtomicInteger count = new AtomicInteger(1);
        System.out.println("Вывод повторяющихся объектов:");
        humanMap.entrySet().stream()
                .filter(s -> s.getValue() > 1)
                .sorted((s1, s2) -> s2.getKey().getAge().compareTo(s1.getKey().getAge()))
                .forEach(s -> System.out.println(("{" + count.getAndIncrement() + ":{" + s.getKey() + "}")));
    }


}
