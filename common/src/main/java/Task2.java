import org.javamoney.moneta.Money;

import javax.money.convert.CurrencyConversion;
import javax.money.convert.MonetaryConversions;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        // для получения курса валют используется библиотека moneta из org.javamoney
        // иногда программа не успевает (или просто не получается) установить соединение с банком для получения курса валют
        // и метод timeRead в NioSocketImpl кидает SocketTimeoutException. К сожалению,
        // я не могу его обработать, из-за чего вывод в консоли не самый красивый,
        // но программа автоматически попытается установить соеднинение заново
        // и конвертация будет произведена
        final String currencies = "[RUB, EUR, USD]";

        Scanner scn = new Scanner(System.in);

        // получаем текущую валюту из доступного списка
        System.out.println("Выберите текущую валюту из следующего списка: " + currencies);
        String currentCurrency = scn.nextLine().toUpperCase();

        // получаем сумму для конвертации
        System.out.println("Введите сумму для конвертации (например 1297,578):");
        Double moneyToConvert = scn.nextDouble();
        scn.nextLine();

        // получаем валюту, в которую будем конвертировать
        System.out.println("Выберите валюту для конвертации: " + currencies);
        String convertTo = scn.nextLine().toUpperCase();

        System.out.println("Пожалуйста, подождите, конвертация займет некоторое время.");

        // конвертация
        CurrencyConversion dollarConversion = MonetaryConversions.getConversion(convertTo);
        System.out.println("\nПеревод осуществлен: " + Money.of(moneyToConvert, currentCurrency).with(dollarConversion));
    }
}
