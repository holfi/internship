CREATE TABLE IF NOT EXISTS locations
(location_id     integer NOT NULL,
 street_address  varchar(40),
 city            varchar(30) NOT NULL,
 PRIMARY KEY     (location_id));

CREATE TABLE IF NOT EXISTS departments
(department_id    integer NOT NULL,
 department_name  varchar(30) NOT NULL,
 manager_id       integer,
 location_id      integer,
 FOREIGN KEY      (location_id)      REFERENCES      locations      (location_id),
 PRIMARY KEY      (department_id));

CREATE TABLE IF NOT EXISTS jobs
(job_id      varchar(10) NOT NULL,
 job_title   varchar(35) NOT NULL,
 min_salary  integer,
 max_salary  integer,
 PRIMARY KEY (job_id));

CREATE TABLE IF NOT EXISTS employees
(employee_id    integer NOT NULL,
 first_name     varchar(20),
 last_name      varchar(25) NOT NULL,
 email          varchar(25) UNIQUE NOT NULL,
 phone_number   varchar(20),
 hire_date      date NOT NULL,
 job_id         varchar(10) NOT NULL,
 salary         integer,
 manager_id     integer,
 department_id  integer,
 commision_pct  integer,
 FOREIGN KEY    (job_id)        REFERENCES      jobs         (job_id),
 FOREIGN KEY    (manager_id)    REFERENCES      employees    (employee_id),
 FOREIGN KEY    (department_id) REFERENCES      departments  (department_id),
 PRIMARY KEY    (employee_id));

CREATE TABLE IF NOT EXISTS job_history
(employee_id    integer NOT NULL,
 start_date     date NOT NULL,
 end_date       date NOT NULL,
 job_id         varchar(10) NOT NULL,
 department_id  integer,
 FOREIGN KEY    (employee_id)   REFERENCES      employees    (employee_id),
 FOREIGN KEY    (job_id)        REFERENCES      jobs         (job_id),
 FOREIGN KEY    (department_id) REFERENCES      departments  (department_id),
 PRIMARY KEY    (employee_id, start_date));