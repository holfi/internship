INSERT INTO locations VALUES
(1, 'Medynskaya 8', 'Moscow'),
(2, 'Dmitrovskaya Plaza 7', 'Moscow');

INSERT INTO jobs VALUES
('IT_PROG', 'Programmer', 8000, 100000),
('DevOps', 'Administrator', 5000, 70000),
('HR', 'HR', 4000, 600000),
('Tester', 'Tester', 5000, 70000);

INSERT INTO departments VALUES
(50, 'IT_PROG', 1, 1),
(20, 'DevOps', 2, 2),
(80, 'HR', 1, 1),
(30, 'Testers', 2, 2);

INSERT INTO employees VALUES
    (1, 'Oleg%', 'Kucherskiy', 'olegkuch@mail.ru', '+79871115554', '14-11-15', 'IT_PROG', 45500, 1, 50, 5000),
    (2, 'Ninno', 'Astashev', 'nastashev@mail.ru', '+79871235554', '23-12-17', 'DevOps', 40100, 2, 20, 4000),
    (3, 'Bagir', 'Marshev', 'bmarshev@mail.ru', '+7991215554', '01-06-18', 'HR', 8500, 1, 80, NULL),
    (4, 'Viktoriotto', 'Petrusenko', 'vpetrusenko@mail.ru', '+79871115554', '12-06-20', 'Tester', 40500, 2, 30, 3000),
    (5, 'Viktor', 'Ruzov', 'vruzov@mail.ru', '+79871115554', '17-03-19', 'Tester', 40500, 2, 30, NULL),
    (6, 'David', 'Imanbekov', 'dimanbekov@mail.ru', '+79871115124', '25-04-18', 'IT_PROG', 50250, 1, 50, 5000),
    (7, 'Anna', 'Risova', 'arisova@mail.ru', '+79876075124', '27-02-17', 'HR', 35000, 1, 80, 1500);
